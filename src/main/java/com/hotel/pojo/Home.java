package com.hotel.pojo;

public class Home {
    private int id;
    private int num;
    private String h_Type;
    private String price;
    private String state;
    private String img;
    private String text;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getH_type() {
        return h_Type;
    }

    public void setH_type(String h_type) {
        this.h_Type = h_type;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Home{" +
                "id=" + id +
                ", num=" + num +
                ", h_type='" + h_Type + '\'' +
                ", price='" + price + '\'' +
                ", state='" + state + '\'' +
                ", img='" + img + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}
