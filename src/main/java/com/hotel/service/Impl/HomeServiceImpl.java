package com.hotel.service.Impl;

import com.hotel.dao.HomeMapper;
import com.hotel.pojo.Home;
import com.hotel.service.HomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class HomeServiceImpl implements HomeService {
    @Autowired
    private HomeMapper homeMapper;

    @Override
    public ArrayList<Home> queryAllHome() {
        return homeMapper.queryAllHome();
    }
}
