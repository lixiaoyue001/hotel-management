package com.hotel.service;

import com.hotel.pojo.Home;

import java.util.ArrayList;

public interface HomeService {

    ArrayList<Home> queryAllHome();
}
