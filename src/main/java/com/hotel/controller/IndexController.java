package com.hotel.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping("/main")
@Controller
public class IndexController {

    /**
     * 进入主页
     * @return
     */
    @RequestMapping("/index.do")
    public String index(){
        System.out.println("index!");
        return "index";
    }

    /**
     * 进入首页
     * @return
     */
    @RequestMapping("/default.do")
    public String default1(){
        System.out.println("default!");
        return "default";
    }

    /**
     * 进入房间管理页面
     * @return
     */
    @RequestMapping("/homeshow.do")
    public String homeshow(){
        System.out.println("homeshow!");
        return "redirect:../home/list.do";
    }

    /**
     * 进入住客入住页面
     * @return
     */
    @RequestMapping("/homeadd.do")
    public String homeadd(){
        System.out.println("homeadd!");
        return "homeadd";
    }

    /**
     * 进入住客列表页面
     * @return
     */
    @RequestMapping("/homelist.do")
    public String homelist(){
        System.out.println("homelist!");
        return "homelist";
    }


}
