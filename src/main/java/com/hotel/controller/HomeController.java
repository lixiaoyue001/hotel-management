package com.hotel.controller;

import com.hotel.pojo.Home;
import com.hotel.service.Impl.HomeServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/home")
public class HomeController {

    @Autowired
    private HomeServiceImpl homeService;

    @RequestMapping("/list.do")
    public ModelAndView list(){
        ModelAndView mv=new ModelAndView();
        List<Home> homeList=homeService.queryAllHome();
        System.out.println(homeList);
        mv.addObject("list",homeList);
        mv.setViewName("homeshow");
        return mv;
    }

}
