package com.hotel.dao;

import com.hotel.pojo.Home;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public interface HomeMapper {
    ArrayList<Home> queryAllHome();
}
