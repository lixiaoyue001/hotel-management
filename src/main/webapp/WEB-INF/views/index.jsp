<%@ page contentType="text/html;charset=UTF-8" language="java" %>

    <html lang="zh">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>后台首页</title>
        <link rel="stylesheet" href="../resource/css/style.css">
        <script src="../resource/js/jquery.min.js"></script>
        <script>
            $(function(){


            })
        </script>
    </head>

    <body>
        <div id="app">
            <!--顶栏-->
            <div class="top">
                <img src="../resource/img/tubiao.png" width="152" height="55" />
            </div>

            <div class="bottom">
                <!-- 左侧 -->
                <ul class="menu">
                    <li><a href="default.do" class="index" target="main">首页</a></li>

                    <li>住客管理</li>
                    <li><a href="homeadd.do" class="sub" target="main">住客入住</a></li>
                    <li><a href="homelist.do" class="sub" target="main">住客列表</a></li>

                    <li>房间管理</li>
                    <li><a href="homeshow.do" class="sub" target="main">房间列表</a></li>

                    <li>会员管理</li>
                    <li><a href="#" class="sub" target="main">新增会员</a></li>
                    <li><a href="#" class="sub" target="main">会员列表</a></li>

                    <li>系统设置</li>
                    <li><a href="#" class="sub" target="main">网站信息</a></li>
                    <li><a href="#" class="sub" target="main">修改密码</a></li>

                    <li>数据表</li>
                    <li><a href="#" class="sub" target="main">生成Excel表</a></li>
                    <li><a href="#" class="exit">退出登录</a></li>
                </ul>
                <!-- 右侧 -->
                <iframe class="right_content" name="main" src="default.do" frameborder="none"></iframe>
            </div>
        </div>
    </body>

    </html>